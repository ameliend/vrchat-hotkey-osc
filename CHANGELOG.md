## [1.2.1](https://gitlab.com/ameliend/vrchat-hotkeys-osc/compare/v1.2.0...v1.2.1) (2022-09-16)


### 🛠 Fixes

* remove margin top for groupbox ([a0f7e12](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/a0f7e12c161ac66e51d7511514dd1690db209e26))

## [1.2.0](https://gitlab.com/ameliend/vrchat-hotkeys-osc/compare/v1.1.1...v1.2.0) (2022-09-16)


### :scissors: Refactor

* Sourcery refactor ([0fae9ae](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/0fae9ae36b22bdf56bb7dcda9daf369612b74540))


### 📔 Docs

* correct spelling ([97b4006](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/97b40062ea4b813c046c241f8ebf079aa40c7482))
* refactor docs ([2753d1e](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/2753d1e7b1a8aaa712f28a953da682336cf56165))


### 🦊 CI/CD

* update pipeline ([b9c3121](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/b9c312122cfce62ce5c7b4696456c62a95db2045))


### 🚀 Features

* Set window fixed size ([c04b107](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/c04b107f6c9e41f34ad07deeac48173c5d28e617))

### [1.1.1](https://gitlab.com/ameliend/vrchat-hotkeys-osc/compare/v1.1.0...v1.1.1) (2022-03-29)


### ⏩ Performance

* sourcery and AI Doc Generator ([61b5041](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/61b5041a09dd4623425901c8d7b213f7437ddee5))
* sourcery and AI Doc Generator ([e0d4731](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/e0d4731583513223e7de6f1167275db175bebc68))


### 📔 Docs

* update banner ([6abe79c](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/6abe79c6365255451f027006d7fd8baed77d12fe))
* update banner ([35c7022](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/35c7022f720f1308f4081e52dc5d72e413ceef5b))
* update docstrings ([e5180a6](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/e5180a65f196867e3c9cc3c20305b64f9e1c9c83))
* update README ([53a3759](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/53a3759122f3d1ce0dcf9bafbff2a193454c12d7))
* update README ([8431d74](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/8431d7492e4bb69133675846ae58b68bf83b0454))
* update README ([d035bb6](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/d035bb6397335e938bacd2b8aade878dd540db67))


### 🦊 CI/CD

* fix version in setup ([b73d7e9](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/b73d7e989ee539854f6c432aee5ed86d1b45f08c))
* fix version in setup ([f075f16](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/f075f169a22190444f3f8e4bae0bc19e968e4994))
* update requirements ([385515c](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/385515c5148323b0f7e8bb43dbec159426f40550))
* update requirements ([77d0ac4](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/77d0ac47a2eabe98e57edc8f3e487e338631b7ed))
* update requirements ([3916686](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/391668602cfe626c076dae8ec1e7df06af6fcf46))
* update requirements ([5707f5e](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/5707f5e46c65dfa99536d25b0f969c192f753d84))
* update pipeline ([822e257](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/822e25759dbd6c9261ad766d36f7acd9e2ee1a15))
* update pipeline ([4f9e51b](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/4f9e51b97cb42a217697b44b2c2ab1a298857687))
* update pipeline ([42aab57](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/42aab57bc9742f3bf8bbbec0939ddef767e5f422))


### Other

* update LICENSE ([66f7aba](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/66f7aba5e0a04e528190e0c0ece49abbf7dfc646))
* update LICENSE ([46549f7](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/46549f7dc8c007f3514fb77cdb1a7230d946e636))

## [1.1.0](https://gitlab.com/ameliend/vrchat-hotkeys-osc/compare/v1.0.0...v1.1.0) (2022-03-05)


### :scissors: Refactor

* module renamed ([99d860e](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/99d860ed6a3e5763dc4cb98d540101418898fd3f))
* reformat ([0df1e12](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/0df1e12c5b90144786044378a412c535d874eb30))
* upgrade Pyside2 to Pyside6 ([819f8fd](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/819f8fd39c70dbe5e66ea310cce8c896a6942ea2))
* upgrade Pyside2 to Pyside6 ([cca653f](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/cca653f48f9de64e8b37963d0a7802e014d50c47))


### 📔 Docs

* add logo icon ([eb19a20](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/eb19a200fd6ad16e4adfd80c5aae9a688ddefa62))
* add screenshot ([81f0a56](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/81f0a56981c07153d54d4a77e85001cdfd2b0521))
* initial doc ([4d92558](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/4d92558f496cdbd50e6e8a48ea9922ecd20008ef))
* update CHANGELOG ([96dce05](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/96dce0516506100b9605e11ae5a6dcd1ef4e3482))
* update docs ([c497462](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/c4974627ec5736702db59f688371c0fe62fb7669))
* update README ([92c036d](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/92c036dcbe6ba501c7c05413b01465a5bacbd6aa))
* update README ([745ad4a](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/745ad4a6c6975e3c6ff5e359f79671bd33e9b924))
* update README ([793df2b](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/793df2b6c1e0e3353f6102703ccf5cbd8c878577))
* update README ([e5439df](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/e5439df0216db05f2b68bb26008af660400d297d))
* update README ([b7c3f93](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/b7c3f93799e1e613bb9826da4aa0032f921897d3))
* update README ([4642367](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/4642367bd340aa850bb1b9ce9616e0747f9f2faf))
* update screenshot ([7898e74](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/7898e74f81bec5504a6e863705227be721a18e98))


### 🦊 CI/CD

* add doc module name ([f08e9f7](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/f08e9f7e5d03896c921d3b3eaa8bbe39d9f4d297))
* docs add viewcode and copybutton ([4c5563a](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/4c5563a2f435b17a51959b6b4ab508ec74b226f6))
* include /build for doc ([d9f5eb8](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/d9f5eb8a2c9639ee0cab3f2693461c003fedbd49))
* update pipeline ([f00b7ad](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/f00b7add5d61090b7b3c1293a14862107089eacc))
* update pipeline ([88185cf](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/88185cfbc7c348b654daaea42080066fa7f24e94))
* with open replaced by red_text() from Pathlib ([1a7600b](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/1a7600b49ad419a8551a13aa6ef46ae0893bd73c))


### 🚀 Features

* add icons ([571d8c2](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/571d8c2e37c1a5d162d41c392bc5a8b78a52a396))
* initial commit ([b08fe35](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/b08fe359b06c6287aa80ae2fe1cd922666da290d))
* work finished and polished for 1st release ([1e375e7](https://gitlab.com/ameliend/vrchat-hotkeys-osc/commit/1e375e779ea8b256d721c6fb0ff984fb7ad80dde))

# 1.0.0 (2022-01-21)

### Features

* initial commit ([3ae6cb9](https://gitlab.com/ameliend/public-python-template/commit/3ae6cb9e802eee298d401fcec261ddef11b8776b))
